package org.example;


import java.time.LocalDate;
import java.util.ArrayList;

public class App
{
    public static void main( String[] args )
    {
        Product p1 = new Product(6745, 5.50, "Penne Pasta");
        Product p2 = new Product(8853, 6.50, "Spaghetti Pasta");
        Product p3 = new Product(2106, 4.50, "Linguine Pasta");
        System.out.println("Total Quantity: " + p3.getTotalQuantity());
////

        Product p = new Product(1234, 9.99, "water");
        FoodProduct p4 = new FoodProduct(3452, 10.0, "Cheddar Cheese",
                LocalDate.parse("2022-06-07"));
        ElectricProduct p5 = new ElectricProduct(4875, 30.0, "Extension cord", "220v");

////
        ////4. Polymorphism “Many Forms”

        ArrayList<Product> products = new ArrayList<>();

        products.add(p);
        products.add(p1);
        products.add(p2);
        products.add(p3);
        products.add(p4);
        products.add(p5);

        for(Product px: products){
            System.out.println(px);

        }

    }
}
