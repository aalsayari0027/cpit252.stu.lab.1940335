package org.example;

public class Product {
    private int id;
    private double price;
    private String name;
    private static int quantity;

    //6. Abstraction
    //too much and unneeded information about products,,
//    private int orderId;
//    private String orderStatus;

    //7. Encapsulation
    //make it private so an outsider cant make the weight by negative and to prevent leaking.
    //public double weight;

    public Product(int id, double price, String name){
        this.id = id;
        this.price = price;
        this.name = name;
        Product.quantity ++;

//        this.orderId = orderId;
//        this.orderStatus = "created"

    }
    public void applySaleDiscount(double percentage){
        this.price = this.price - ((percentage/100) * this.price);
    }
        //5. Controlling Changes
    public final void addToShoppingCart(){
        System.out.println(this.name + " has been added to the shopping cart.");
    }

    public int getTotalQuantity(){
        return Product.quantity;
    }

    public void getSellableStatus(){
        System.out.println("This product is sellable");
    }

    public String toString(){
        return "Product info:\n+Id: " + this.id + "\t" + "name: " + this.name +
                "\tPrice: SR" + this.price;
    }
}
